$(document).ready(function () {

  $('#logo-img').hide();
  $('#spring').hide();

  $(function() {
    $('#navig').affix({
      offset: {
        top: 700
      }
    });
    $('#logo-img').show();

  });

  $('.slider').slick({
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: false,
    infinite: true,
    fade: true,
    slide: 'div',
    arrow: false,
    cssEase: 'linear'
  }
);
$('.slider2').slick({
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  dots: false,
  infinite: true,
  fade: true,
  slide: 'div',
  arrow: false,
  cssEase: 'linear'
});

$('#headline').height($("#child").height());


$('a.spring-link').click(function() {
  $("#fall").hide();
  $("#spring").show();
});
$('a.fall-link').click(function() {
  $("#spring").hide();
  $("#fall").show();
});
$(".dropdown-menu a").click(function() {
  $(this).closest(".dropdown-menu").prev().dropdown("toggle");
});
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});


});

/*
$(function(){

$('#logo').affix({
offset:{
top: 300
}
});
$('#navig').css("margin-top","600");
});
$('#child').height($("#nephew").height());
*/
